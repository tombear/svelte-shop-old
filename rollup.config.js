const FS = require("fs");
const SVGO = require("svgo");
const svgO = new SVGO();

import svelte from "rollup-plugin-svelte";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import livereload from "rollup-plugin-livereload";
import { terser } from "rollup-plugin-terser";
import json from "rollup-plugin-json";
import svgo from "rollup-plugin-svgo";

const production = !process.env.ROLLUP_WATCH

const readFilePromise = (path) => {
    return new Promise((resolve, reject) => {
        FS.readFile(path, "utf-8", (err, data) => {
            if (err) reject(err);
            resolve(data);
        })
    })
}

const writeFilePromise = (path, data) => {
    return new Promise((resolve, reject) => {
        FS.writeFile(path, data, "utf-8", (err) => {
            if (err) reject(err);
            resolve(data);
        })
    })
}

const itemsJson = require("./items.json").map(async itemJson => {
    const imagePath = itemJson["imagePath"];
    delete itemJson.imagePath;
    const fileData = await readFilePromise(imagePath);
    const svgData = await svgO.optimize(fileData);
    itemJson.data = {
        svg: svgData.data
    }
    return itemJson;
})

const configP = async function() {
    const newJson = await Promise.all(itemsJson);
    await writeFilePromise("./public/initState.json", JSON.stringify(newJson));
    return {
        input: 'src/main.js',
        output: {
            sourcemap: true,
            format: 'iife',
            name: 'app',
            file: './public/bundle.js'
        },
        plugins: [
            json({
                include: "./public/initState.json",
                compact: true,
            }),
            svgo(),
            svelte({
                dev: !production,
                css: css => {
                    css.write('./public/bundle.css');
                }
            }),
            resolve(),
            commonjs(),
            !production && livereload('./public'),
            production && terser()
        ]
    };
}

export default configP;
