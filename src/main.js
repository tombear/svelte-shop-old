import initState from "../public/initState.json";
import TShirtShop from "./components/TShirtShop.svelte";

const app = new TShirtShop({
    target: document.body,
    props: {
        tshirts: initState
    }
});

export default app;
